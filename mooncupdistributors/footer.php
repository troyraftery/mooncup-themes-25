<?php
/**
 * Mooncup Main template for displaying the footer
 *
 * @package WordPress
 * @subpackage Mooncup Main
 * @since Mooncup Main 1.0
 */
?>
				<div class="pre-footer container_boxed">
					 <?php echo do_shortcode('[gallery link="none" ids="237,211,185,159" columns="4"]'); ?>
				</div>

				<div class="mailing-list bg-blue text-reverse">
					<div class="container_boxed">
						<div class="mcwp-form white-lined">
							<?php sutb_form_captureform(false);?>
						</div>
					</div>
				</div>
				<footer class="footer">
					<ul class="footer-widgets container_boxed"><?php
						if ( function_exists( 'dynamic_sidebar' ) ) :
							dynamic_sidebar( 'footer-sidebar' );
						endif; ?>
					</ul>

					<div class="credits-container">
						<div class="container_boxed">
							<div class="credits-links">
								<?php
								wp_nav_menu( array('menu' => 'footer-terms' ));
								?>
							</div>
							<div class="credits-neptik">
								<p>&copy; Copyright 2002-<?php echo date("Y");?> Mooncup Ltd / Site designed and developed by <a href="http://www.freshrecipe.co.uk/" title="Fresh Recipe" target="_blank">Mooncup</a></p>
							</div>
						</div>
					</div>
				</footer>

			</div><!--end of site-->



			<script>


		<?php wp_footer(); ?>

	</body>
</html>
