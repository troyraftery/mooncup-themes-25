<?php
/**
 * @package WordPress
 * @subpackage Mooncup Main
 * @since Mooncup Main 1.0
 * Template Name: Find Online REtailer
 */

get_header(); ?>

<section class="find-online-page page-content primary" role="main" >
	<div class="container_boxed content_band--small">

		<div class="page-head">
			<?php
			the_field('1col_content_area');
			?>
		</div>

		<?php
		if ( have_posts() ) : the_post();

			get_template_part( 'loop' ); ?>

			<aside class="post-aside"><?php

			wp_link_pages(
					array(
							'before'           => '<div class="linked-page-nav"><p>' . sprintf( __( '<em>%s</em> is separated in multiple parts:', 'mooncupmain' ), get_the_title() ) . '<br />',
							'after'            => '</p></div>',
							'next_or_number'   => 'number',
							'separator'        => ' ',
							'pagelink'         => __( '&raquo; Part %', 'mooncupmain' ),
					)
			); ?>

			<?php
			if ( comments_open() || get_comments_number() > 0 ) :
				comments_template( '', true );
			endif;
			?>

			</aside><?php

		else :

			get_template_part( 'loop', 'empty' );

		endif;
		?>
		<form class="find-stockist" method="get" action="/buy-the-mooncup/find-an-online-retailer/">
			<div class="location-search">
			<label class="blue-caps"><?php _e('Select your country','mooncupmain');?>:</label>
			<select name="online-location">
				<?php
				$country = new WC_Countries;
				$countries = $country->get_allowed_countries();
				$taxonomy_c = 'online-stockists';
				$tax_terms = get_terms($taxonomy_c);
				if(isset($_COOKIE['location'])):
				$location_iso = $_COOKIE['location'];
				foreach($countries as $key =>$value) {
				    if($location_iso == $key):
				    foreach ($tax_terms as $tax_term) {
				        if($tax_term->name == $value):
				        $location_name = $value;
				        $_COOKIE['locationname'] = $value;
				        break;
				        else:
				        $location_name = '';
				        endif;
				    }
				    endif;
				}
				else:
				$location_name = 'United Kingdom (UK)';
				endif;
				$taxonomy = 'online-stockists';
				$tax_terms = get_terms($taxonomy);
				foreach ($tax_terms as $tax_term) {
				    if($tax_term->name == $_GET['online-location']):
					   echo '<option selected>'.$tax_term->name.'</option>';
				    else:
				        echo '<option>'.$tax_term->name.'</option>';
				    endif;
				}
				?>
			</select>
			</div>
			<input type="submit" value="Search"/>
		</form>

		<div class="retailer-listing">
			<?php
			if($_GET['online-location'] == 'United Kingdom (UK)'){
				$loc = 'United Kingdom';
			}else{
			$loc = $_GET['online-location'];
		};
			$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
			$args = array(
					'post_type' => 'stockists',
					'posts_per_page' => 10,
                 'paged' => $paged,
					'tax_query' => array(
							array(
									'taxonomy' => 'online-stockists',
									'field' => 'slug',
									'terms' => $loc
							),
					),
			);
			$query = new WP_Query( $args );
			$max_page = $query->max_num_pages;
			while ($query->have_posts()) : $query->the_post();
				get_template_part( 'loop', 'link' );
			endwhile;

			?>
            <?php   

$big = 999999999; // need an unlikely integer

echo paginate_links( array(
	'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
	'format' => '?paged=%#%',
	'current' => max( 1, get_query_var('paged') ),
	'total' => $query->max_num_pages
) );		

wp_reset_postdata();

?>
		</div>
	</div>
</section>



<?php get_footer(); ?>
