<?php
/**
 *
 * @package WordPress
 * @subpackage Mooncup Main
 * @since Mooncup Main 1.0
 * Template Name: Meet the team
 */

get_header(); ?>
<section class="single-col meet-the-team page-content primary" role="main">
		
	        <article class="container_full splash-content-block">
	        	<div class = "splash-image-narrow splash-image_generic image_fullwidth" style="background-image:url('<?php the_field('splash_image'); ?>');">
		        	<div class="splash-content-overlay splash-header text-reverse">
		        		<div class="container_full">
			        	<?php the_field('splash_content'); ?>
			        	</div>
		        	</div>
		        </div>
		    </article>

		    
	        <article class="container_boxed content_band--small">
	        		<?php the_field('1col_content_area');?>
	        </article>
			
			<div class="container_boxed">
			<?php

			// check if the repeater field has rows of data
			if( have_rows('team_member') ):?>
			

			    <?php while ( have_rows('team_member') ) : the_row();?>
				
				<div class="team-member">
					<div class = "team-member__image">
					    <img src="<?php the_sub_field('head_shot');?>" alt="Team Member Image">
					</div>
						        	
					<div class="team-member__name center">
						<h3><?php
							the_sub_field('team_name');
						?>
						</h3>
					</div>

					<div class="team-member__job-title center">
						<?php
							the_sub_field('job_title');
						?>
					</div>

					<div class="grid-content">
						<?php
						the_sub_field('team_bio');
						?>
					</div>
				</div>
						       
			    <?php endwhile;?>
			</div>
			<?php 

			else :

			    // no rows found

			endif;

			?>

			<aside class="container_boxed content_band--lined">
	        	<div class="container_boxed--narrow content_band--small">
	        		<?php the_field('footer_area');?>
	        	</div>
	        </aside>
	
</section>

<?php get_footer(); ?>
