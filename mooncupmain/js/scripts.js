(function($) {
	$(document).ready(function() {
	  var menuToggle = $('#js-mobile-menu').unbind();
	  $('.navigation-menu').removeClass("show");

	  menuToggle.on('click', function(e) {
	    e.preventDefault();
	    $('.navigation-menu').slideToggle(function(){
	      if($('.navigation-menu').is(':hidden')) {
	        $('.navigation-menu').removeAttr('style');
	      }
	    });
	  });

	  var menuToggle = $('#js-mobile-menu-small').unbind();
	  $('.navigation-menu-small').removeClass("show");

	  menuToggle.on('click', function(e) {
	    e.preventDefault();
	    $('.navigation-menu-small').slideToggle(function(){
	      if($('.navigation-menu-small').is(':hidden')) {
	        $('.navigation-men-small').removeAttr('style');
	      }
	    });
	  });

	  //initiate sliderbx
	    $('.slider-horizontal').bxSlider({
		    slideWidth: 405,
		    minSlides: 1,
		    maxSlides: 3,
		    infiniteLoop: false,
  			hideControlOnEnd: true,
		    slideMargin: 15
		});

		$('.story-slider').bxSlider({
		    slideWidth: 620,
		    minSlides: 1,
		    maxSlides: 2,
		    infiniteLoop: false,
  			hideControlOnEnd: true,
		    slideMargin: 15
		});

		$('.blog-slider').bxSlider({
		    slideWidth: 2000,
		    auto:true,
		    minSlides: 1,
		    maxSlides: 1,
		    infiniteLoop: false,
		    speed:1000,
  			hideControlOnEnd: true
		    
		});
	
	    var ship_change = 0;
	    $("select[name='wcpbc-manual-country']").on('change',function(){
			//if(ship_change>1){
				var r = confirm("Please confirm you wish to change location as this will reset your cart?");
				if (r == true) {
					old_url = window.location.href;
					if($(this).val()=='IT' && old_url != 'http://91.239.125.102/italy/'){
					_url = 'http://91.239.125.102/italy/';
					}  else if($(this).val()=='Other' && old_url != 'http://91.239.125.102/mcuk/'){
					_url = 'http://91.239.125.102/mcuk/';
					}   else if($(this).val()=='IL'){
					_url = 'http://mooncup.co.il';
					}   else if($(this).val()=='HR'){
					_url = 'http://mooncup.co.hr';
					} else {

					_url = old_url.substring(0, old_url.indexOf('?'));
					}
					window.location.href = _url+'?geocode=' + location+'&empty-cart=true';
					var date = new Date();

					date.setTime(date.getTime()+(2*864000*1000));
					document.cookie = 'location ='+location+'; expires='+date.toGMTString()+';path=/;';
				}
			//}
				//ship_change++;
        });
				$('.button-primary').on('click',function(){
					var r = confirm("Please confirm you wish to change location as this will reset your cart?");
					if (r == true) {
						old_url = window.location.href;
						_url = old_url.substring(0, old_url.indexOf('?'));
						document.location.href = _url+'?geocode=' + location+'&empty-cart=true';
						var date = new Date();

						date.setTime(date.getTime()+(2*864000*1000));
						document.cookie = 'location ='+location+'; expires='+date.toGMTString()+';path=/;';
					}
	        });
			
		var ship_change = 0;
	    $('#shipping_country').on('change',function(){
			//if(ship_change>1){
				var r = confirm("Please confirm you wish to change location as this will reset your cart?");
				if (r == true) {
					old_url = window.location.href;
					if($(this).val()=='IT' && old_url != 'http://91.239.125.102/italy/'){
					_url = 'http://91.239.125.102/italy/';

					} else if($(this).val()=='IL'){
					_url = 'http://mooncup.co.il';
					}   else if($(this).val()=='HR'){
					_url = 'http://mooncup.co.hr';
					}  else {
					_url = old_url.substring(0, old_url.indexOf('?'));
					}
					document.location.href = _url+'?geocode=' + location+'&empty-cart=true';
					var date = new Date();

					date.setTime(date.getTime()+(2*864000*1000));
					document.cookie = 'location ='+location+'; expires='+date.toGMTString()+';path=/;';
				}
			//}
				ship_change++;
        });	

	    $( "iframe.youtube-player" ).wrap( "<div class='videoWrapper'></div>" );

	    $(window).load(function() {
		   $('.splash-content-overlay h1').each(function(i) {
		      $(this).delay((i + 0) * 10).fadeIn(500);
		   });
		});

	    $( ".benefit-expand-trigger" ).click(function() {
		  $( ".home-benefit__full" ).slideToggle( "slow", function() {
		    $("html, body").animate({ scrollTop: $(".benefit-group").offset().top }, 500);
		    console.log('executed scrollToElement');
		    return true;
		  });
		});

	    //placeholders for emial inputs
		$("#sutb_form_email").attr("placeholder", "Enter your email address");

		$("#sutb_form_firstname").attr("placeholder", "First Name");

		$("#sutb_form_lastname").attr("placeholder", "Last Name");



		$('select[name=billing_customer_info]').change(function () {
		    if ($(this).val().toLowerCase().indexOf('please specify')>-1) {
		        $('input[name=billing_please_specify_here]').attr("required", "required");
		    } else {
		        //alert('I dont need to be mandatory');
		        $('input[name=billing_please_specify_here]').removeAttr("required");
		    }
		});

		$('.validate-email label:contains("Repeat value")').empty().prepend("Confirm Email Address*");

		//script for mobile reordering
		
		 if ($(window).width() < 769) {
		      $('.sidebar.col__4').insertAfter($('.testimonial-content'));
		  }
		  else {
		      $('.testimonial-content').insertAfter($('.sidebar.col__4'));
		  }

		  $(window).resize(function() {
		     if ($(window).width() < 769) {
		      $('.sidebar.col__4').insertAfter($('.testimonial-content'));
		  }
		  else {
		      $('.testimonial-content').insertAfter($('.sidebar.col__4'));
		  }
		  });

		  if ($(window).width() < 769) {
		      $('.sidebar.col__4').insertAfter($('.faq-content'));
		  }
		  else {
		      $('.faq-content').insertAfter($('.sidebar.col__4'));
		  }

		  $(window).resize(function() {
		     if ($(window).width() < 769) {
		      $('.sidebar.col__4').insertAfter($('.faq-content'));
		  }
		  else {
		      $('.faq-content').insertAfter($('.sidebar.col__4'));
		  }
		  });
	
		   /*Thanks to CSS Tricks for pointing out this bit of jQuery
			http://css-tricks.com/equal-height-blocks-in-rows/
			It's been modified into a function called at page load and then each time the page is resized. One large modification was to remove the set height before each new calculation. */

			equalheight = function(container){

			var currentTallest = 0,
			     currentRowStart = 0,
			     rowDivs = new Array(),
			     $el,
			     topPosition = 0;
			 $(container).each(function() {

			   $el = $(this);
			   $($el).height('auto')
			   topPostion = $el.position().top;

			   if (currentRowStart != topPostion) {
			     for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
			       rowDivs[currentDiv].height(currentTallest);
			     }
			     rowDivs.length = 0; // empty the array
			     currentRowStart = topPostion;
			     currentTallest = $el.height();
			     rowDivs.push($el);
			   } else {
			     rowDivs.push($el);
			     currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
			  }
			   for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
			     rowDivs[currentDiv].height(currentTallest);
			   }
			 });
			}

			$(window).load(function() {
			  equalheight('ul.products li');
			});


			$(window).resize(function(){
			  equalheight('ul.products li');
			});


			$(function() {
			  //$('nav--blog-category li a[href^="/' + location.pathname.split("/")[1] + '"]').addClass('active');
			  if ((location.pathname.split("/")[1]) !== ""){
			        $('nav--blog-category li a[href^="/' + location.pathname.split("/")[1] + '"]').addClass('active');
			    }
			});

			// single blog page scroll to content
			if ($('#blog-single').length > 0) { 
			   $('html, body').animate({
			        scrollTop: $('.blog-single').offset().top
			    }, 'slow');
			}

	});
})(jQuery);
