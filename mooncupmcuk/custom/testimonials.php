<?php
/**
 * @package WordPress
 * @subpackage Mooncup Main
 * @since Mooncup Main 1.0
 * Template Name: Testimonials LP
 */

get_header(); ?>
<section class="single-col using-your-mooncup page-content primary" role="main">

	        <article class="container_full splash-content-block">
	        	<div class = "splash-image-narrow splash-image_generic image_fullwidth" style="background-image:url('<?php the_field('splash_image'); ?>');">
		        	<div class="splash-content-overlay splash-header text-reverse">
		        		<div class="container_boxed">
			        	<?php the_field('splash_image_overlay'); ?>
			        	</div>
		        	</div>
		        </div>
		    </article>

	        	<section class="container_boxed content_band">
	        		<aside class="sidebar col__4">
	        		<ul class="sidebar"><?php
						if ( function_exists( 'dynamic_sidebar' ) ) :
							dynamic_sidebar( 'testimonials-sidebar' );
						endif; ?>
					</ul>
	        		</aside>



	        		<article class="testimonial-content col__8">
	        			<?php the_field('testimonials_area_intro'); ?>
	        			<div class="testimonial-archive container">
	        			<?php

						$post_objects = get_field('testimonials_listing');

						if( $post_objects ): ?>

						    <?php foreach( $post_objects as $post): // variable must be called $post (IMPORTANT) ?>
						        <?php setup_postdata($post); ?>
						        <article class="testimonial testimonial-post">

										<div class="testimonial-image">
											<?php if (has_post_thumbnail( $post->ID ) ): ?>
												<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
									        	<div class = "content post-featured-image image-cover" style="background-image:url('<?php echo $image[0]; ?>');"></div>
										    <?php endif; ?>
										</div>

										<div class="post-content">
											<blockquote>
												<?php the_content(); ?>
												<a href="<?php the_permalink(); ?>" class="read-more"><?php _e( 'Read more', 'mooncupmain' ); ?></a>
											</blockquote>
											<p class="caps small"><?php the_title(); ?></p>


										</div>
								</article>
						    <?php endforeach; ?>

						    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
						<?php endif;?>


	        			</div>
	        		</article>



	        	</section>

</section>


<?php get_footer(); ?>
